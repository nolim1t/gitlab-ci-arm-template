FROM ubuntu
COPY hello.sh /root/hello.sh
WORKDIR /root
ENTRYPOINT ["/root/hello.sh"]
